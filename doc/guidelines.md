Coding Guidelines
=================

We try to _somewhat_ stick to
[PEP 8](https://www.python.org/dev/peps/pep-0008/). There are minor
contradictions to PEP 8 in this code which have been found to be more annoying
than useful. Probably not everything is listed, here, but definitely the most
important points:

* _Note: very short und rough list_
* All functions and classes (besides function “main”) should start with a
  single underscore as the only function meant to be called from outside is the
  “main” function.
* Everything should come with a docstring. Exception is when the name of the
  node is so clear that it would not be necessary at all.
* Constants should be in capital letters with single underscores between words;
  e.g. SOME_CONSTANT
* Class names should be in camel case with a capital letter at the beginning;
  e.g. SomeClass
* All other names should be small letters with single underscores between the
  different words. Abbreviations might be in capital letters, though; e.g.
  calc_RMS
* If you introduce a new option, implement a corresponding check for it in
  function “_check_options”!
* Comments can go up to the full maximum line length of 79 characters.
